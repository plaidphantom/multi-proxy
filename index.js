const http = require('http');
const httpProxy = require('http-proxy');
const open = require('opn');

const args = process.argv.slice(2);
const target = args[0];
const proxyPorts = args[1].split(/\D/);

const servers = proxyPorts.map(port => {
    const proxy = httpProxy.createProxyServer();

    proxy.on('error', function(err, _req, res) {
        console.warn(`Error in port ${port} proxy:`, err);

        if (!res.headersSent)
           res.writeHead(504, { 'content-type': 'application/json' });

        res.end(JSON.stringify({ error: 'proxy_error', reason: err.message }));
    });

    const proxyServer = http.createServer(function(req, res) {
        proxy.web(req, res, { target: `http://${target}:${port}`, ws: true });
    });

    proxyServer.on('upgrade', function(req, socket, head) {
        console.log(`proxying websocket to port ${port}`)
        proxy.ws(req, socket, head, { target: `http://${target}:${port}`, ws: true });
    });

    proxyServer.listen(port);

    console.log(`listening on port ${port}...`);

    return proxyServer;
})

open(`http://localhost:${proxyPorts[0]}/`);

process.on('SIGINT', function() {
    console.log('Shutting down...');
    servers.forEach(s => s.close());
    console.log('All servers stopped.');
    process.exit();
});
