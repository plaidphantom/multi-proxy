# Multi Proxy

A simple script to proxy multiple ports at once from another machine and make them available on localhost. This is mostly made for debugging web apps and APIs locally while being accessed on another machine (such a VM).

The actual apps will need to be listening on whatever IP you access them over, so one solution is to use "http://*:PORT/" or "0.0.0.0" or whatever your particular server uses to make it available on all interfaces.

## Usage

Suppose a Web App on `localhost:4000` talking to an API on `localhost:4300`. After getting the apps listening on all interfaces, `git clone` this onto the client, run `npm install`, and use this command:

	node index.js MY.OTHER.IP.ADDRESS 4000,4300

The script assumes the first port is the web app and open it in your default browser.

This can support any number of comma-separated port numbers.

This proxy assumes HTTP, if anyone wants HTTPS support, PRs are welcome!

## License

This is made available under a [BSD license](https://opensource.org/licenses/BSD-3-Clause). See LICENSE.md.
